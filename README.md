**Magic Crystal Adventure**

A top-down 2D game about magic and crystals and maybe dragons too.

Programmed in Python and using the arcade/pyglet modules for OpenGL drawing and media.

I will need to learn about shaders since when I last looked at OpenGL, glPushMatrix and glRotatef were pretty much the definitive way to program an engine.

Also want to use good software practices throughout:
 - Black auto-formatter
 - Unittests for small chuck logic
 - Integration tests for bigger pieces
