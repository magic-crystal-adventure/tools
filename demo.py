import arcade
import datetime
import pyglet
from pyglet.gl import *

GAME_TITLE = "Magic Crystal Adventure"
GAME_WIDTH = 1024
GAME_HEIGHT = 768
W = GAME_WIDTH - 1
H = GAME_HEIGHT - 1
GAME_UPDATE_DELTA = 1 / 30
UPDATE_RATE = 1 / 120

TILE_WIDTH = 64.0
TILE_HEIGHT = 64.0


class ObjectTestRenderer:
    def __init__(self, lib):
        self.lib = lib
        # self.foreground = pyglet.graphics.OrderedGroup(1)
        self.vertex_list = None

    def setup(self, batch):

        colors = []
        points = []

        # fmt: off
        points.extend([-10.0,-10.0,0.0,10.0]) # top-left
        points.extend([0.0, 10.0,10.0,-10.0]) # top-right
        points.extend([10.0,-10.0,-10.0,-10.0]) # bottom

        colors.extend([255,0,0,255,255,0,0,255])
        colors.extend([255, 0, 0, 255, 255, 0, 0, 255])
        colors.extend([128, 128, 128, 255, 128, 128, 128, 255])
        # fmt: on

        N = len(points) // 2

        self.vertex_list = pyglet.graphics.vertex_list(
            N, ("v2f", tuple(points)), ("c4B", tuple(colors)),
        )

    def render(self, state):
        x = state.cur_state[0]
        y = state.cur_state[1]
        glPushMatrix()
        glLoadIdentity()
        glTranslatef(x, y, 0.0)
        self.vertex_list.draw(GL_LINES)
        glPopMatrix()


class GridRenderer:
    def __init__(self, lib):
        self.lib = lib
        self.background = pyglet.graphics.OrderedGroup(0)

    def setup(self, batch):
        width = float(self.lib.state.horizontal_tiles * TILE_WIDTH)
        height = float(self.lib.state.vertical_tiles * TILE_HEIGHT)

        colors = []
        points = []
        # horizontal lines
        for i in range(self.lib.state.vertical_tiles + 1):
            y = i * TILE_HEIGHT
            points.extend([0.0, y, width, y])
            colors.extend([255] * 8)
        # vertical lines
        for i in range(self.lib.state.horizontal_tiles + 1):
            x = i * TILE_WIDTH
            points.extend([x, 0.0, x, height])
            colors.extend([255] * 8)

        N = len(points) // 2

        batch.add(
            N,
            GL_LINES,
            self.background,
            ("v2f", tuple(points)),
            ("c4B", tuple(colors)),
        )

    def render(self, state):
        pass


class MultiStageRenderer:
    def __init__(self, lib):
        self.lib = lib
        self.renderers = []
        self.batch = pyglet.graphics.Batch()

    def add_renderer(self, renderer):
        self.renderers.append(renderer)

    def setup(self):
        for renderer in self.renderers:
            renderer.setup(self.batch)

    def render(self, state):
        # TODO: mixing paradigms here...
        self.batch.draw()
        for renderer in self.renderers:
            renderer.render(state)


class State:
    def __init__(self, lib):
        self.lib = lib
        self.horizontal_tiles = 10
        self.vertical_tiles = 10
        self.fwd_state = [100.0, 100.0]
        self.old_state = None
        self.cur_state = None
        self.pixelsPerSec = 24.0

    def update(self, update_delta):
        # copy old state
        self.old_state = self.fwd_state[:]

        # advance the game by update_delta
        movement_x = self.pixelsPerSec * update_delta
        movement_y = self.pixelsPerSec * update_delta / 2.0

        self.fwd_state[0] += movement_x
        self.fwd_state[1] += movement_y

        self.cur_state = self.fwd_state[:]

    def interpolate(self, alpha):
        # interpolate the currentState / previousState by alpha
        self.cur_state[0] = self.old_state[0] * (1.0 - alpha)
        self.cur_state[0] += self.fwd_state[0] * alpha

        self.cur_state[1] = self.old_state[1] * (1.0 - alpha)
        self.cur_state[1] += self.fwd_state[1] * alpha


class AccumulatorStepper:
    def __init__(self, lib, update_delta):
        self.lib = lib
        self.update_delta = update_delta
        self.accumulator = self.update_delta # force initial update

    def update(self, delta_time: float):
        self.accumulator += delta_time

        while self.accumulator >= self.update_delta:
            # step the game by the update delta
            self.lib.state.update(self.update_delta)
            self.accumulator -= self.update_delta

        # interpolate state by time left in accumulator
        alpha = self.accumulator / self.update_delta
        self.lib.state.interpolate(alpha)


class Lib:
    def __init__(self):
        self.stepper = AccumulatorStepper(self, GAME_UPDATE_DELTA)
        self.state = State(self)
        self.renderer = MultiStageRenderer(self)


class Window(arcade.Window):
    """ The game window """

    def __init__(self, lib, width, height, title):
        super().__init__(
            width, height, title, antialiasing=False, update_rate=UPDATE_RATE
        )
        self.counter = pyglet.window.FPSDisplay(window=self)
        self.lib = lib
        self.fps = None
        self.clock = pyglet.clock.Clock()
        self.clock.tick()

    def on_draw(self):
        delta_time = self.clock.tick()
        self.lib.stepper.update(delta_time)  # meant to happen in on_update but oh well
        arcade.start_render()
        # glMatrixMode(GL_PROJECTION)
        # glLoadIdentity()
        # glOrtho(0, GAME_WIDTH, 0, GAME_HEIGHT, -1, 1)
        # glTranslatef(0.25, 0.25, 0.0)
        # glMatrixMode(GL_MODELVIEW)
        glLoadIdentity()
        glTranslatef(0.25, 0.25, 0.0)
        self.lib.renderer.render(self.lib.state)
        self.counter.draw()

    # def on_update(self, delta_time: float):
    #    super().on_update(delta_time) # required?
    #    self.lib.stepper.update(delta_time)


def main():
    lib = Lib()
    window = Window(lib, GAME_WIDTH, GAME_HEIGHT, GAME_TITLE)
    grid_renderer = GridRenderer(lib)
    obj_renderer = ObjectTestRenderer(lib)
    lib.renderer.add_renderer(grid_renderer)
    lib.renderer.add_renderer(obj_renderer)
    lib.renderer.setup()
    arcade.run()


if __name__ == "__main__":
    main()
